"""
### EMEARALD PIPELINE ###
classify_texts.py classifies texts into 3 biome environments using random forest models and tfidf/doc2vec as features
# @author: Maaly Nassar
# @email:maaly13@yahoo.com
"""

import os
from os import path
import sys
import stat
import re
import json
import string
import pathlib
from pathlib import Path
from glob import glob
import pandas as pd
import numpy as np
import spacy
nlp = spacy.load("en_core_sci_lg")
nlp.max_length = 10000000
import nltk
nltk.download('stopwords')
stopwords = nltk.corpus.stopwords.words('english')
import joblib
import pickle
import time
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier # random forest classifier
from gensim.models.doc2vec import Doc2Vec, TaggedDocument

### shell parameters ###

print(sys.argv)
#sys.argv[0] is the name of this file 
txt_dir = sys.argv[1] # path to the texts directory
model_path = sys.argv[2] # path to random forest model
features_type = sys.argv[3] # tfidf or doc2vec
vmodel = sys.argv[4] # doc2vec model path if feature_type is doc2vec else "None"
prob_lm = sys.argv[5] # prediction probability threshold e.g. 0.5, 0.4 
return_pred_text = sys.argv[6] # output the predicted text with its class, values could be: yes or no
output_dir = sys.argv[7]

### model prediction function ###

def clean_text(text):        
    text = ''.join([word.lower() for word in text if word.lower() not in string.punctuation and word.lower() not in stopwords])
    words = [word.text for word in nlp(text)]      
    return words  

def predict_texts_classes(txt_dir,model_path,features_type,vmodel,prob_lm,return_pred_text):    

    ### generate dataframe for texts ###
    txtfs = Path(txt_dir).glob('**/*.txt')        
    data = {}        
    for col in ['id','text']:
        data[col] = []            

    for i, f in enumerate(txtfs):
        pmcid = re.sub(r'.txt','',f.parts[-1])            
        data['id'].append(pmcid)
        data['text'].append(f.read_text())                                       
    data = pd.DataFrame(data)
    upmcs = list(np.unique(data['id']))
    data = data[data['id'].isin(upmcs)]
    data = data[data['text']!='']       
    print(data)

    ### loading models ###

    if features_type == 'tfidf':
        tfidf_vect = TfidfVectorizer(analyzer=clean_text)   

    if features_type == 'doc2vec':
        dvmodel = Doc2Vec.load(vmodel) # loading doc2vec model

    [features,rf_model,X_test,Y_test] = joblib.load(model_path) # random forest trained model

    ### predict texts - 1000 texts/prediction run ###

    pred_df = pd.DataFrame()        
    splt = 1000
    start = list(range(0,len(data),splt))
    end = [*list(range(splt,len(data),splt)),*[len(data)]]

    for n,m in enumerate(start):

        start_time = time.time()

        T_features = pd.DataFrame()            

        if features_type == 'tfidf':
            T_tfidf = tfidf_vect.fit_transform(data.iloc[m:end[n]]['text'].values.astype('U'))# values.astype('U') np.nan error
            T_features = pd.DataFrame(T_tfidf.toarray())
            T_features.columns = tfidf_vect.get_feature_names()
            T_features = T_features.reindex(features,axis=1,fill_value=0)

        if features_type == 'doc2vec':                
            for t,txt in enumerate(data.iloc[m:end[n]]['text']):                      
                if features_type == 'doc2vec':
                    txtwords = [word.text for word in nlp(txt)]
                    txtv = dvmodel.infer_vector(txtwords)
                vdf = pd.DataFrame(txtv).transpose() 
                #print(wvdf)
                T_features = T_features.append(vdf)          

        elapsed_time = time.time() - start_time
        print(features_type+' features generation time:'+str(round(elapsed_time/60,3)))

        YT_pred = {}
        YT_pred['id'] = data.iloc[m:end[n]]['id']
        if return_pred_text == 'yes': YT_pred['text'] = data.iloc[m:end[n]]['text']
        YT_pred['label'] = rf_model.predict(T_features)
        YT_pred['prob'] = list(rf_model.predict_proba(T_features))           
        YT_pred['prob'] = [round(probs[list(rf_model.classes_).index(YT_pred['label'][p])],2) for p,probs in enumerate(YT_pred['prob'])] 
        YT_pred_df = pd.DataFrame(YT_pred)
        YT_pred_df = YT_pred_df[YT_pred_df['prob']>float(prob_lm)]
        YT_pred_df = YT_pred_df.sort_values(by=['prob'],ascending=False)
        print(YT_pred_df)

        pred_df = pred_df.append(YT_pred_df)            

        elapsed_time = time.time() - start_time
        print('model prediction time:'+str(round(elapsed_time/60,3)))          

    pred_df = pred_df.sort_values(by=['prob'],ascending=False) 
    pred_df = pred_df.reset_index(drop=True)

    return pred_df 


### run model prediction ###
if ~Path(output_dir).exists():os.system('mkdir -m 750 '+output_dir)
pred_df = predict_texts_classes(txt_dir,model_path,features_type,vmodel,prob_lm,return_pred_text)
pred_df = pred_df.sort_values(by='prob').reset_index(drop=True)
pred_df.to_csv(Path(output_dir,features_type+'_texts_classes.csv'),encoding='utf-8',index=False)