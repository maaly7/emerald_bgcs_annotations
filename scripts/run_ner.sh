#!/bin/sh

##########################
#### EMERALD PIPELINE ####
##########################
# @author: Maaly Nassar
# @email:maaly13@yahoo.com
##########################

####### PREDICT NER - USING TENSORFLOW METAGENOMICS NER MODELS #######
######################################################################

### SHELL ARGUMENTS ###
ner_dir=$1
python_path=$2

pred_input_dir="$ner_dir/input"
pred_model_dir="$ner_dir/models"

gs_csv="$ner_dir/models/ner-grid-metrics.csv" #grid search high precision models file
bert_path="scripts/bert" #bert script path
biobert_model_path="$ner_dir/models/biobert_v1.0_pubmed_pmc" #biobert model

logs_dir="logs";if [ ! -d $logs_dir ]; then mkdir $logs_dir; fi

exec < $gs_csv || exit 1
read header # read (and ignore) the first line

while IFS=, read -r entity data splt lr epc r p f;do

entity=${entity}
data=${data}
epc=${epc%%.*}
splt=${splt%%.*}
lr="$lr"

entity_input_dir="$pred_input_dir/$entity"
entity_model_dir="$pred_model_dir/$entity/$lr-$epc"

### PREDCIT ###

echo "$entity annotations started ... "

$python_path $bert_path/run_ner.py --task_name=NER --max_seq_length=128 --learning_rate=$lr --do_train=false --do_predict=true --do_eval=false --vocab_file=$biobert_model_path/vocab.txt --bert_config_file=$biobert_model_path/bert_config.json --init_checkpoint=$biobert_model_path/biobert_model.ckpt --num_train_epochs=$epc --data_dir=$entity_input_dir --output_dir=$entity_model_dir 1> $logs_dir/run_ner_output.log 2> $logs_dir/run_ner_error.log 3> $logs_dir/run_ner_console.log

$python_path $bert_path/biocodes/ner_detokenize.py --token_test_path=$entity_model_dir/token_test.txt --label_test_path=$entity_model_dir/label_test.txt --answer_path=$entity_input_dir/test.tsv --output_dir=$entity_model_dir 1> $logs_dir/run_ner_output.log 2> $logs_dir/run_ner_error.log 3> $logs_dir/run_ner_console.log

perl $bert_path/biocodes/conlleval.pl < $entity_model_dir/NER_result_conll.txt 1> $logs_dir/run_ner_output.log 2> $logs_dir/run_ner_error.log 3> $logs_dir/run_ner_console.log

echo "$entity annotations are completed successfully ... "
echo "####### "

done

