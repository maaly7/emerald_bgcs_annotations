"""
### EMEARALD PIPELINE ###
run_ner_process.py runs ner_process class functions to preprocess texts for metagenomics NER models and postprocess metagenomics annotations
# @author: Maaly Nassar
# @email:maaly13@yahoo.com
"""


import sys
import os
from pathlib import Path
from bert import tokenization #to import tokenization from subdirectories, empty __init__.py was created in ml folder
from ner_process import ner_process
ner_process = ner_process()

### SHELL ARGUMENTS ###
ner_dir = sys.argv[1]
do = sys.argv[2]
txt_dir = sys.argv[3]

print([ner_dir,do,txt_dir])

### LOADING TOKENIZER ###
vocab_file = 'entity_classifier/models/biobert_v1.0_pubmed_pmc/vocab.txt'
tokenizer = tokenization.FullTokenizer(vocab_file=vocab_file, do_lower_case=False)

### RUN NER PROCESS FUNCTIONS ###
if do == 'bio-text':
    ### bio-text: sentencise and tokenize texts in BIO schema ###
    entities=['bgc-gene-name','bgc-gene-product-name','bgc-organism','bgc-compound','bgc-accession','bgc-action','bgc-class']
    pred_input_dir=Path(ner_dir,'input')
    ner_process.bio_tag_predict(txt_dir,entities,pred_input_dir,tokenizer) 

if do == 'process-ner':
    ### process-ner: postprocess and normalize annotations ###
    output_dir = sys.argv[4]
    if not os.path.isdir(Path(output_dir)):os.mkdir(Path(output_dir),0o775)
    predict_models_dir = Path(ner_dir,'models')
    ner_process.process_biobert_predict_dataset(txt_dir,tokenizer,predict_models_dir,output_dir)
    
