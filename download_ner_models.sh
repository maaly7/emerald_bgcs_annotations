#!/bin/sh

entities=(bgc-accession bgc-action bgc-class bgc-compound bgc-gene-name bgc-gene-product-name bgc-organism)
accessions=(MODEL2203040001 MODEL2203040002 MODEL2203040003 MODEL2203040004 MODEL2203040005 MODEL2203040006 MODEL2203040007)

for i in ${!entities[@]};do
echo ${entities[$i]}
echo ${accessions[$i]}
models_dir="entity_classifier/models"
dzip="$models_dir/${entities[$i]}.zip"
if [ -f ${dzip} ]; then rm -rf ${dzip}; fi
if [ -d $models_dir/${entities[$i]} ]; then rm -rf $models_dir/${entities[$i]}; fi
wget -O ${dzip} https://www.ebi.ac.uk/biomodels/model/download/${accessions[$i]}?filename=${entities[$i]}.zip
unzip $dzip -d $models_dir
if [ -f ${dzip} ]; then rm -rf ${dzip}; fi
done
