## **EMERALD BIOSYNTHETIC GENE CLUSTERS (BGCs) ANNOTATIONS** 

- This repository was developed for the [EMERALD](https://gtr.ukri.org/projects?ref=BB%2FS009043%2F1) project to enrich [MiBiG](https://mibig.secondarymetabolites.org/) database with BGCs metadata from literature, using machine and deep learning models. The enriched metadata includes BGC organisms, genes, gene-products, accessions, compounds, compound actions (e.g. antitumor, antiviral, antibacterial) and compound chemical classes (e.g. alkaloid, saccharide).   

- This repository offers BGCs classifiers that classifies literture into BGC and NO-BGC, 7 BGCs Named-Entity-Recognition (NER) models that annotate BGCs related metadata in texts and BGCs classifiers that discover novel BGCs in whole genome sequences. 

#### **System Requirements**

- This repository requires a computer with `RAM: 16GB`, `CPU: 6vCore`, `SSD: 30GB`. This version has been tested on Linux operating system and [Google Colab](https://colab.research.google.com) .

#### **Installation**
  
  - Clone emerald_bgcs_annotations repository:
  
    ```
    $ git clone https://gitlab.com/maaly7/emerald_bgcs_annotations.git
    $ cd emerald_bgcs_annotations
    ```
    ##### _*If you are using Google Colab, you need to run `%cd emerald_bgcs_annotations` to change your current Colab directory to `emerald_bgcs_annotations`._    
     
  - Install miniconda, if you don't have anaconda or python already installed:
  
    ```
    $ wget https://repo.anaconda.com/miniconda/Miniconda3-py39_4.11.0-Linux-x86_64.sh    
    $ bash Miniconda3-py39_4.11.0-Linux-x86_64.sh
    $ source ~/.bashrc
    ```
    
    ##### _*On executing `bash Miniconda3-latest-Linux-x86_64.sh`, you need to type yes when requried. `source ~/.bashrc` will be required to add conda aliases and paths to your shell environment. If you are using Google Colab, you can skip installing miniconda._ 
            
  - Create bgcs conda environment:
  
    ```
    $ conda create -n bgcs python=3.7
    ```
    ##### _*If you are using Google Colab, you can skip creating bgcs environment._
     
  - Activate bgcs environment and install python packages:
  
    ```
    $ conda activate bgcs
    $ pip install -r requirements.txt
    ``` 
    ##### _*If you are using Google Colab, you can skip activating bgcs environment and install the requirements directly._
    
  - Download and unzip NER models directly into `entity_classifier/models` by running the following command (This step takes ~15-30 min):
  
    ```
    $ bash download_ner_models.sh
    ```
    
    ##### _*You can also download and upload them manually from BioModels - see the table below._
    

#### **Classifying BGCs Literature**

  - You can classify literature into `bgc` or `no-bgc` using the random forest (rf) classifiers in `text_classifier/models` folder. 
  
  - Two models were trained on the publications listed in `text_classifier/input/randomforest/rf_data_bgcs_no_bgcs_all_r7_n348.csv`:
  
      - doc2vec random forest classifer `text_classifier/models/randomforest/rf_7_150_25_k5_n348_t0.2_d0_doc2vec_all_model.pkl` was trained on publications Doc2Vec generated from doc2vec model `text_classifier/models/doc2vec/bgcs_no_bgcs_doc2vec_200_5.model`. 
      
      - tfidf random forest classifer `text_classifier/models/randomforest/rf_7_150_25_k5_n348_t0.2_d0_tfidf_all_model.pkl` was trained on publications TF-IDF. 
  
  - To classify texts, run the following command with the following arguments:
  
      ```
      $ python scripts/classify_texts.py
          <path to your texts directory>
          <random forest model path> - tfidf or doc2vec random forest classifer path  
          <feature_type> - tfidf or doc2vec
          <doc2vec model path> - None if tfidf random forest classifer is selected and doc2vec model path if doc2vec is selected
          <prediction probability threshold>  - 0.4 or 0.5
          <return predicted text in the predictions files> - yes or no 
          <path to output directoy>
      ```
  
  - For example, run the following command to classify texts examples in `texts` using tfidf random forest classifier and predictions will be saved in csv files in `text_classifier/output` :
  
      ```
      $ python scripts/classify_texts.py texts text_classifier/models/randomforest/rf_7_150_25_k5_n348_t0.2_d0_tfidf_all_model.pkl tfidf None 0.5 no text_classifier/output
      ```

  - For example, run the following command to classify texts examples in `texts` using Doc2Vec random forest classifier and predictions will be saved in csv files in `text_classifier/output` ::

      ```
      $ python scripts/classify_texts.py texts text_classifier/models/randomforest/rf_7_150_25_k5_n348_t0.2_d0_doc2vec_all_model.pkl doc2vec text_classifier/models/doc2vec/bgcs_no_bgcs_doc2vec_200_5.model 0.5 no text_classifier/output
      ```   

#### **Annotating Literature with BGCs Entities**

  - You can annotate BGCs literature with 7 novel BGCs entities using the BGCs Named-Entity-Recognition (NER) models you downloaded into `entity_classifier/models` folder.
  
  - 7 BGCs NER models were trained on the curated datasets in `entity_classifier/input/dataset.csv` to identify and annotate metadata related to the following entities:
  
      | Entity | Definition | BioModels Download URLs |
      | ---      | ---      | ---      | 
      | 1. _**bgc-accession**_   | BGC organism   | [MODEL2203040001](https://www.ebi.ac.uk/biomodels/MODEL2203040001)   |
      | 2. _**bgc-action**_   | BGC compound action   | [MODEL2203040002](https://www.ebi.ac.uk/biomodels/MODEL2203040002)   |
      | 3. _**bgc-class**_   | BGC compound chemical class   | [MODEL2203040003](https://www.ebi.ac.uk/biomodels/MODEL2203040003)   |
      | 4. _**bgc-compound**_   | BGC compound   | [MODEL2203040004](https://www.ebi.ac.uk/biomodels/MODEL2203040004)   |
      | 5. _**bgc-gene-name**_   | BGC gene   | [MODEL2203040005](https://www.ebi.ac.uk/biomodels/MODEL2203040005)   |
      | 6. _**bgc-gene-product-name**_   | BGC gene product   | [MODEL2203040006](https://www.ebi.ac.uk/biomodels/MODEL2203040006)   |
      | 7. _**bgc-organism**_   | BGC organism   | [MODEL2203040007](https://www.ebi.ac.uk/biomodels/MODEL2203040007)   |
  
  - **To annotate texts, you need to**:
      
      1. Preprocess and tokenize your texts (e.g. `texts` folder) for the 7 BGCs NER models:
      
          ```
          $ python scripts/run_ner_process.py entity_classifier bio-text texts 
          ```
          
          ##### _*The `texts` argument can be changed to the path to your texts folder. This command will generate tokenized sentences in each model folder in `entity_classifier/input` folder._
          
          
      2. Annotate preprocessed texts with each of the 7 BGCs NER models (10 texts examples can take from ~20-60 min on CPU and ~8-10 min on [Google Colab](https://colab.research.google.com) GPU):
      
          ```
          $ bash scripts/run_ner.sh entity_classifier python
          ``` 
          
          ##### _*The `python` argument can be changed to your conda environment python path, e.g., `<path to your miniconda>/bgcs/python`._
          
      3. Postprocess and normalize BGCs annotations to ontologies:
      
          ```
          $ python scripts/run_ner_process.py entity_classifier process-ner texts entity_classifier/output 
          ``` 
          
          ##### _*The `texts` and `entity_classifier/output` can be changed to the path to your texts and output folders, respectively. This command will generate your final normalized annotations in json files for download in `entity_classifier/output/sent-json` (Europe PMC sentence annotations format) and `entity_classifier/output/ent-json` (Europe PMC entity annotations format)._           


#### **Citation** 

  - For usage of BGCs annotations repository, please cite this Gitlab repository    
  

#### **Contact information** 

  - For help or issues using BGCs annotations repository, please contact Maaly Nassar (maaly13@yahoo.com). 
